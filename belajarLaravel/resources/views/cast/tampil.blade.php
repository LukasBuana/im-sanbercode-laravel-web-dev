@extends('layouts.master')
@section('title')
Halaman Tambah Cast
    
@endsection
@section('sub-title')
Halaman Cast   
@endsection
@section('content')
<a href="/cast/create" class="btn btn-info btn-sm">Tambah</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
        <tr>
            <th scope="row">{{ $key +1 }}</th>
            <td>{{$item->nama}}</td>
            <td>{{$item->umur}}</td>
            <td>{{$item->bio}}</td>
            <td>
              <form action="/cast/{{$item->id }}" method="post">
                <a href="/cast/{{$item->id }}" class="btn btn-info btn-sm" >Detail</a>
                <a href="/cast/{{$item->id }}/edit" class="btn btn-warning btn-sm" >Edit</a>
                  @csrf
                  @method('delete')
                  <input type="submit" value="delete" class="btn btn-danger btn-sm">
                </form>
            </td>
        </tr> 
        @empty
            <h1>Data Kosong</h1>
        @endforelse
     
      
    </tbody>
  </table>
@endsection