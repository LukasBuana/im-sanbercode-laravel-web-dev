@extends('layouts.master')
@section('title')
Halaman Biodata
    
@endsection
@section('sub-title')
Biodata    
@endsection
@section('content')
     <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
    <label for="name">First name:</label><br><br>
    <input type="text"name="fname"><br><br>
    <label for="name">Last name:</label><br><br>
    <input type="text"name="lname"><br><br>
    <label for="gender">Gender:</label><br><br>
    <input class="radio" type="radio" name="radio">Male <br>
    <input class="radio" type="radio" name="radio">Female <br>
    <input class="radio" type="radio" name="radio">Other <br><br>
    <label for="national">Nationality:</label><br><br>
    <select name="country" id="">
        <option value="option">Indonesian</option>
        <option value="option">Singaporean</option>
        <option value="option">Malaysian</option>
        <option value="option">Australian</option>
    </select><br><br>
    <label for="language">language Spoken:</label><br><br>
    <input type="checkbox" name="spoken">Bahasa Indonesia <br>
    <input type="checkbox" name="spoken">English <br>
    <input type="checkbox" name="spoken">Other <br><br>
    <label for="bio">Bio:</label><br><br>
    <textarea name="message" id="" cols="30" rows="10"></textarea><br>
    <input  type="submit" name="SignUp" value="Sign Up">
    </form>
   
@endsection
   