<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class , 'utama']);
Route::get('/biodata', [AuthController::class , 'bio']);

Route::post('/welcome', [AuthController::class , 'send']);

Route::get('/table', function(){
    return view('page.table');
});
Route::get('/data-table', function(){
    return view('page.data-table');
});
Route::get('/cast/create', [CastController::class, 'create']);
Route::post('/cast', [CastController::class , 'store']);
Route::get('/cast', [CastController::class , 'index']);
Route::get('/cast/{id}', [CastController::class , 'show']);

Route::get('/cast/{id}/edit', [CastController::class , 'edit']);    
Route::put('/cast/{id}', [CastController::class , 'update']);

Route::delete('/cast/{id}', [CastController::class , 'destroy']);
