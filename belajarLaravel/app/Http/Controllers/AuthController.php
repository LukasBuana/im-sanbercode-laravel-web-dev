<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio()
    {
        return view('page.biodata');
    }

    public function send(Request $request)
    {
        $namaDepan= $request['fname']; 
        $namaBelakang= $request['lname']; 

        return view('page.home', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
